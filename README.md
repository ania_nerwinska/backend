# 'backend'

'backend' is new micro service
Backend app

## Setting up the project
Please delete this part after setting up whole project 

### Setting up repository:
1. create bitbucket repository:
https://bitbucket.org/repo/create with name backend for Provide bitbucket workspace name organization and Provide bitbucket workspace name project

2. enable bitbucket pipelines
https://bitbucket.org/Provide bitbucket workspace name/backend/admin/addon/admin/pipelines/settings

3. Set up ssh keys for the repository 
generate ssh key
https://bitbucket.org/Provide bitbucket workspace name/backend/admin/addon/admin/pipelines/ssh-keys

add generated public key to the, this will allow to have access to all repositories from bitbucket pipeline
https://bitbucket.org/account/user/Provide bitbucket workspace name/ssh-keys/

4. Add repo notifications to  #Provide bitbucket workspace name-bot channel
https://bitbucket.org/Provide bitbucket workspace name/backend/admin/addon/admin/bitbucket-chats-integration/repo-config-admin

### Setting up the routing:
Add a new dispatcher to the router project in https://bitbucket.org/Provide bitbucket workspace name/router/src/develop/dispatch.yaml
```yaml
    - url: "*/backend_app/v1*"
    service: backend_app
``` 

and then deploy the router project

### Setting the init version
1. Delete Readme about project setup
2. Initialize git and add origin:
```bash
git init
git add .
git commit -m 'Init API'
git remote add origin git@bitbucket.org:Provide bitbucket workspace name/backend_app.git
git push -u origin develop
```

## Local usage
1. (optional) If you want to use custom firebase cert you need to create file
`firebase-cert.json` in main folder with the certificate

Building the project

```bash
make build-dev
```

Starting the app (with docker)
```bash
make start
```

Reinitializing the database
```bash
make init-db
```

Running tests
```bash
make tests
```

## Deployment
Deployments are triggered when you create a new release-* tag (for example release-1.0.0).
Beta deployment is completely automatic and to deploy to prod you need to click a button in the BitBucket pipeline.

## Connecting to the production db
You can use for that sql proxy, instruction under this link:
https://cloud.google.com/python/django/appengine

##Logging
Loggers are logging to stackdriver logging and stackdriver error reporting when project is deployed to beta and prod.
Error reporting is getting logs on level ERROR and higher.
Locally loggers are logging only to local python console.