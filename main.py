# entry point for google cloud engine

from wsgi import application as app

# dirty hack to run migrations on gcloud sql db
# the other option is to connect from bitbucket using sql proxy and run migrate command
from django.core.management import call_command
call_command("migrate", interactive=False)
