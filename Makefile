start:
	docker-compose up -d

stop:
	docker-compose down

build-dev:
	pip install -r requirements.txt
	python manage.py collectstatic --noinput
	docker-compose build
	make update-db

init-db:
	make start
	docker-compose exec web ./wait-for-it.sh -h db -p 5432 -s && sleep 5
	docker-compose exec web python manage.py migrate
	docker-compose exec web python manage.py flush --noinput

update-db:
	make start
	docker-compose exec web ./wait-for-it.sh -h db -p 5432 -s && sleep 5
	docker-compose exec web python manage.py migrate

tests:
	docker-compose run --rm web pytest