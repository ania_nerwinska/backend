from django.contrib.auth.models import User
from django.db import models

STATUS_CHOICES = [("new", "new"), ("assigned", "assigned"), ("resolved", "resolved")]


class Request(models.Model):
    needs_firebrigade = models.BooleanField(default=False)
    needs_ambulance = models.BooleanField(default=False)
    disaster_type = models.CharField(max_length=1024)
    assigned_user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    phone_number = models.CharField(blank=True, null=True, max_length=1024)
    location = models.CharField(blank=True, null=True, max_length=1024)
    status = models.CharField(choices=STATUS_CHOICES, default="new", max_length=1024)