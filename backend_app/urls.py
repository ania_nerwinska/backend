from django.conf.urls import url
from django.urls import path, include
from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter

from backend_app.views.request import RequestCreateView, RequestViewSet

api_urls = []
default_router = DefaultRouter()
default_router.register(r'request', RequestViewSet, basename='request')
api_urls += default_router.urls

urlpatterns = [
    url(r'', include(api_urls)),
    path('request-create/', RequestCreateView.as_view()),
    path('login/', views.obtain_auth_token),
]
