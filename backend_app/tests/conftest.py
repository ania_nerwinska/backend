import pytest
from unittest import mock

from django.contrib.auth.models import User
from rest_framework.test import APIClient

from .fixtures import *


@pytest.fixture
def user():
    return User(username='123', email='user@example.com')


@pytest.fixture()
def rest_client():
    return APIClient()


@pytest.fixture()
def auth_client(user, rest_client):
    rest_client.force_authenticate(user)
    return rest_client

