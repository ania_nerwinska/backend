from rest_framework import viewsets, mixins, generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from backend_app.models import Request
from backend_app.serializers.request import RequestSerializer


class RequestCreateView(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = RequestSerializer
    queryset = Request.objects.all()


class RequestViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    serializer_class = RequestSerializer
    queryset = Request.objects.all()
