"""
Django settings for backend_app project.

Generated by 'django-admin startproject' using Django 2.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""
import json
import os
import sys

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Add local libs to path
sys.path.insert(0, os.path.join(BASE_DIR, 'lib'))

# Import it after adding local lib folder
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-#dxz4=+pgl#t8!w_&0s(vd%xjbeidredivo5(jm@e%5r9j@m)'

DEBUG = False
LOCAL = False

# SECURITY WARNING: don't run with debug turned on in production!
if not os.environ.get('GAE_APPLICATION') or os.environ.get('BACKEND_DEBUG'):
    DEBUG = True

# SECURITY WARNING: run it only locally, developers debug for queries
if not os.environ.get('GAE_APPLICATION'):
    LOCAL = True


INTERNAL_IPS = ['127.0.0.1', 'localhost']
ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
]
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'backend_app',

    'drf_yasg',
    'rest_framework',
    'rest_framework.authtoken'

]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[{server_time}] {message}',
            'style': '{',
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', ],
            'level': 'INFO',
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
        'backend_app': {
            'handlers': ['console', ],
            'level': 'INFO',
        }
    }
}

if not LOCAL:
    from google.cloud import logging as google_logging

    client = google_logging.Client()
    client.setup_logging()

    LOGGING['handlers']['stackdriver'] = {
        'class': 'backend_app.helpers.custom_logging_handler.CustomErrorReportingGCloudHandler',
        'client': client
    }

    LOGGING['loggers']['backend_app'] = {
            'handlers': ['stackdriver'],
            'level': 'INFO',
    }
    LOGGING['loggers']['django'] = {
            'handlers': ['stackdriver'],
            'level': 'INFO',
    }


DEFAULT_RENDERER_CLASSES = (
    'rest_framework.renderers.JSONRenderer',
)
if DEBUG:
    DEFAULT_RENDERER_CLASSES = DEFAULT_RENDERER_CLASSES + (
        'rest_framework.renderers.BrowsableAPIRenderer',
    )

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': DEFAULT_RENDERER_CLASSES
}

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'description': 'Our firebase api token, used to authenticate, Bearer prefix required before token',
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    }
}

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',  # FIXME: Should only be added in dev env
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

]

CORS_ORIGIN_WHITELIST = [
    'http://localhost:3000',
    'http://127.0.0.1:3000',
    'https://beta-app.ihroma.it',
    'https://backend.oa.r.appspot.com',
]

CORS_ALLOW_HEADERS = [
    '*',
]

# FIXME do we want to check CORS?
CORS_ORIGIN_ALLOW_ALL = True

if LOCAL:
    MIDDLEWARE.append('querycount.middleware.QueryCountMiddleware')

ROOT_URLCONF = 'urls'
WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('BACKEND_DB_NAME', 'backend'),
        'USER': os.environ.get('BACKEND_DB_USER', 'backend'),
        'PASSWORD': os.environ.get('BACKEND_DB_PASSWORD', 'backend'),
        'HOST': os.environ.get('BACKEND_DB_HOST', 'localhost'),
        'PORT': os.environ.get('BACKEND_DB_PORT', '5432'),
        'CONN_MAX_AGE': 60,  # 1 minute
    }
}

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/backend_app/v1static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'OPTIONS': {
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
        ],
        'loaders': [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ],
    },
}]
